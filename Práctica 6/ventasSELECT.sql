-- Eduardo Jiménez Bahíllo

-- 3.1. Devuelve un listado con todas las compras que se han realizado. Las compras deben estar ordenadas por la fecha de 
-- realización, mostrando en primer lugar las compras más recientes.
SELECT * FROM pedido ORDER BY fecha DESC;
-- 3.2. Devuelve todos los datos de las dos compras de mayor valor.
SELECT * FROM pedido ORDER BY total DESC LIMIT 2;
-- 3.3. Devuelve un listado con los identificadores de los compradores que han realizado alguna compra. Tenga en cuenta 
-- que que no debe mostrar identificadores que estén repetidos.
SELECT DISTINCT comprador FROM pedido;
-- 3.4. Devuelve un listado de todas las compras que se realizaron durante el año 2020, cuya cantidad sea superior a 100€.
SELECT * FROM pedido WHERE YEAR(fecha)=2020 AND total>100;
-- 3.5. Devuelve un listado con el nombre y los apellidos de los agentes que tienen una comisión entre 0.02 y 0.12.
SELECT nombre, apellido1, apellido2 FROM agente WHERE comision BETWEEN 0.02 AND 0.12;
-- 3.6. Devuelve el valor de la comisión de mayor valor que existe en la tabla agente.
SELECT MAX(comision) Comision_mayor FROM agente;
-- 3.7. Devuelve el id, nombre y 1er. apellido de aquellos compradores cuyo segundo apellido no es NULL. El listado deberá 
-- estar ordenado alfabéticamente por apellidos y nombre.
SELECT id, nombre, apellido1 FROM comprador WHERE apellido2 IS NOT NULL ORDER BY apellido1, nombre;
-- 3.8. Devuelve un listado de los nombres de los compradores que empiezan por R y terminan por l y también los nombres 
-- que empiezan por E. El listado deberá estar ordenado alfabéticamente.
SELECT nombre FROM comprador WHERE nombre LIKE "R%l" OR nombre LIKE "E%" ORDER BY nombre;
-- 3.9. Devuelve un listado de los nombres de los compradores que no comienzan por H. El listado deberá estar ordenado alfabéticamente.
SELECT nombre FROM comprador WHERE nombre NOT LIKE "H%" ORDER BY nombre;
-- 3.10. Devuelve un listado con el primer apellido de los agentes que terminan por or ó om. Tenga en cuenta que se deberán 
-- eliminar los apellidos repetidos.
SELECT DISTINCT apellido1 FROM agente WHERE apellido1 LIKE "%or" OR apellido1 LIKE "%om";