-- Eduardo Jiménez Bahíllo

USE compras;

INSERT INTO comprador 
(id,nombre,apellido1,apellido2,ciudad,categoria) VALUES
(1,"Raúl","Gómez","Salas","Almería",100),
(2,"Francisco Javier","Rivera","Díaz","Granada",200),
(3,"Adrián","Santos","Moreno","Sevilla",null),
(4,"Yassin","Abdeselam","Hamed","Jaén",300),
(5,"Noel","Santos","Garzón","Almería",200),
(6,"Raquel","Mendieta","Salas","Cádiz",100),
(7,"Ildefonso","Moreno","Sánchez","Sevilla",300),
(8,"Eduardo","Pérez","Moreno","Huelva",200),
(9,"Nizar","Mohamed",null,"Granada",225),
(10,"Bilal","Abdeselam",null,"Sevilla",125);

INSERT INTO agente
(id,nombre,apellido1,apellido2,comision) VALUES
(1,"Oliver","Johnson","Smith",0.15),
(2,"Harry","Brown",null,0.13),
(3,"Jacob","Davis","Miller",0.11),
(4,"Charlie","Beckham","Kane",0.14),
(5,"Thomas","Taylor",null,0.12),
(6,"George","Wilson","Moore",0.13),
(7,"William","Baines","Johnson",0.11),
(8,"Jack","Jones",null,0.05);

INSERT INTO pedido
(comprador,agente,total,fecha) VALUES
(1,1,220.99,"2019-12-28"),
(1,2,50,"2019-12-30"),
(2,5,15.33,"2020-01-10"),
(3,7,154.84,"2020-01-12"),
(4,2,457,"2020-01-24"),
(7,8,10.25,"2019-12-30"),
(9,6,0.99,"2020-02-05"),
(10,7,18.15,"2020-02-06"),
(2,4,20.20,"2020-02-09"),
(6,3,19.99,"2020-02-13"),
(10,5,24.95,"2020-02-24"),
(6,3,2.50,"2020-02-28"),
(8,3,6.51,"2020-02-28"),
(8,4,81.15,"2020-03-01"),
(2,2,9.99,"2020-03-02");