-- Eduardo Jiménez Bahíllo

DROP DATABASE IF EXISTS compras;
CREATE DATABASE compras;
USE compras;

CREATE TABLE IF NOT EXISTS agente (
	id INT UNSIGNED AUTO_INCREMENT,
	nombre VARCHAR(100) NOT NULL,
	apellido1 VARCHAR(100) NOT NULL,
	apellido2 VARCHAR(100),
	comision DOUBLE,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS comprador (
	id INT UNSIGNED AUTO_INCREMENT,
	nombre VARCHAR(100) NOT NULL,
	apellido1 VARCHAR(100) NOT NULL,
	apellido2 VARCHAR(100),
	ciudad VARCHAR(100),
	categoria INT UNSIGNED,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS pedido (
	id INT UNSIGNED AUTO_INCREMENT,
	total DOUBLE,
	fecha DATE,
	agente INT UNSIGNED NOT NULL,
	comprador INT UNSIGNED NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (agente) REFERENCES agente(id),
	FOREIGN KEY (comprador) REFERENCES comprador(id)
);