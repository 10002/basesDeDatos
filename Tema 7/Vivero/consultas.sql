-- Realiza una consulta con el nombre, apellidos y puesto de aquellos empleados que no
-- sean representantes de ventas.
SELECT nombre, apellido1, apellido2, puesto FROM empleado WHERE puesto!="Representante Ventas";
-- Realiza una consulta con el nombre, apellidos y email de los empleados cuyo jefe
-- tiene un código de jefe igual a 7.
SELECT nombre, apellido1, apellido2, email FROM empleado WHERE codigo_jefe=7;
-- Realiza una consulta con todos los productos que pertenecen a la gama Ornamentales
-- y que tienen más de 100 unidades en stock. El listado deberá estar ordenado por su
-- precio de venta, mostrando en primer lugar los de mayor precio.
SELECT * FROM producto WHERE gama="Ornamentales" AND cantidad_en_stock>100 ORDER BY precio_venta DESC;
-- Realiza una consulta con el código de cliente de aquellos clientes que realizaron
-- algún pago en 2008. Tenga en cuenta que deberá eliminar aquellos códigos de cliente
-- que aparezcan repetidos (Utilizando la función YEAR de MySQL).
SELECT DISTINCT codigo_cliente FROM pago WHERE YEAR(fecha_pago)=2008;
-- Realiza una consulta con el código de oficina y la ciudad donde hay oficinas.
SELECT codigo_oficina, ciudad FROM oficina;
-- Realiza una consulta con el código de pedido, código de cliente, fecha esperada
-- y fecha de entrega de los pedidos cuya fecha de entrega ha sido al menos dos días
-- antes de la fecha esperada.
SELECT codigo_pedido, codigo_cliente, fecha_esperada, fecha_entrega FROM pedido WHERE (fecha_esperada-fecha_entrega)>=2;
-- Realiza una consulta con el código de pedido, código de cliente, fecha esperada
-- y fecha de entrega de los pedidos cuya fecha de entrega ha sido al menos dos días
-- antes de la fecha esperada (Utilizando la función DATEDIFF de MySQL).
SELECT codigo_pedido, codigo_cliente, fecha_esperada, fecha_entrega FROM pedido WHERE DATEDIFF(fecha_esperada,fecha_entrega)>=2;
-- Realiza una consulta con todos los clientes que sean de la ciudad de Madrid y cuyo
-- representante de ventas tenga el código de empleado 11 ó 30.
SELECT * FROM cliente WHERE ciudad="Madrid" AND codigo_empleado_rep_ventas IN (11,30);
-- Realiza una consulta con los distintos estados, sin que aparezcan repetidos,
-- por los que puede pasar un pedido.
SELECT DISTINCT estado FROM pedido;
-- Realiza una consulta con el código de pedido, código de cliente, fecha esperada
-- y fecha de entrega de los pedidos cuya fecha de entrega ha sido al menos dos días
-- antes de la fecha esperada (Utilizando la función ADDDATE de MySQL).
SELECT codigo_pedido, codigo_cliente, fecha_esperada, fecha_entrega FROM pedido WHERE ADDDATE(fecha_entrega,2)<=fecha_esperada;
-- Realiza una consulta con todos los pagos que se realizaron en el año 2008 mediante
-- PayPal. Ordene el resultado de mayor a menor.
SELECT * FROM pago WHERE YEAR(fecha_pago)=2008 AND forma_pago="PayPal" ORDER BY total DESC;
-- Realiza una consulta con la ciudad y el teléfono de las oficinas de España.
SELECT ciudad, telefono FROM oficina WHERE pais="España";
-- Realiza una consulta con el nombre de los todos los clientes españoles (El dominio
-- del campo país está escrito en inglés).
SELECT nombre_cliente FROM cliente WHERE pais="Spain";
-- Consulta el nombre del puesto, nombre, apellidos y email del jefe de la empresa.
SELECT puesto, nombre, apellido1, apellido2, email FROM empleado WHERE codigo_jefe IS NULL;
-- Realiza una consulta de todos los pedidos que han sido entregados en el mes de
-- enero de cualquier año.
SELECT * FROM pedido WHERE MONTH(fecha_entrega)=1;
-- Realiza una consulta con el código de cliente de aquellos clientes que realizaron
-- algún pago en 2008. Tenga en cuenta que deberá eliminar aquellos códigos de cliente
-- que aparezcan repetidos (Utilizando la función DATE_FORMAT de MySQL).
SELECT DISTINCT codigo_cliente FROM pago WHERE DATE_FORMAT(fecha_pago, "%Y")=2008;
-- Realiza una consulta con el código de pedido, código de cliente, fecha esperada
-- y fecha de entrega de los pedidos que no han sido entregados a tiempo.
SELECT codigo_pedido, codigo_cliente, fecha_esperada, fecha_entrega FROM pedido WHERE fecha_entrega<=fecha_esperada;
-- Realiza una consulta con todas las formas de pago que aparecen en la tabla pago.
-- Tenga en cuenta que no deben aparecer formas de pago repetidas.
SELECT DISTINCT forma_pago FROM pago;
-- Realiza una consulta con el código de pedido, código de cliente, fecha esperada
-- y fecha de entrega de los pedidos que no han sido entregados a tiempo.
SELECT codigo_pedido, codigo_cliente, fecha_esperada, fecha_entrega FROM pedido WHERE fecha_entrega>fecha_esperada;
-- Realiza una consulta con el código de cliente de aquellos clientes que realizaron
-- algún pago en 2008. Tenga en cuenta que deberá eliminar aquellos códigos de cliente
-- que aparezcan repetidos (Sin utilizar ninguna de las funciones YEAR ó DATE_FORMAT).
SELECT DISTINCT codigo_cliente FROM pago WHERE fecha_pago LIKE "2008-%";
-- Realiza una consulta de todos los pedidos que fueron rechazados en 2009.
SELECT * FROM pedido WHERE estado="Rechazado" AND YEAR(fecha_pedido)=2009;
