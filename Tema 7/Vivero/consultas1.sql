-- Realiza una consulta con el nombre de los clientes que han hecho pagos 
-- y el nombre de sus representantes junto con la ciudad de la oficina a 
-- la que pertenece el representante.
SELECT DISTINCT c.nombre_cliente, e.nombre, o.ciudad 
FROM cliente c, empleado e, pago p, oficina o
WHERE c.codigo_empleado_rep_ventas=e.codigo_empleado AND e.codigo_oficina=o.codigo_oficina AND c.codigo_cliente=p.codigo_cliente;
-- Realiza una consulta con el nombre de los clientes que no hayan hecho 
-- pagos y el nombre de sus representantes junto con la ciudad de la oficina 
-- a la que pertenece el representante.
SELECT c.nombre_cliente, e.nombre, o.ciudad 
FROM cliente c, empleado e, oficina o
WHERE c.codigo_empleado_rep_ventas=e.codigo_empleado AND e.codigo_oficina=o.codigo_oficina 
AND c.codigo_cliente NOT IN (SELECT codigo_cliente FROM pago);
-- Realiza una consulta con el nombre de los clientes que no hayan 
-- realizado pagos junto con el nombre de sus representantes de ventas.
SELECT c.nombre_cliente, e.nombre 
FROM cliente c, empleado e
WHERE c.codigo_empleado_rep_ventas=e.codigo_empleado AND c.codigo_cliente NOT IN (SELECT codigo_cliente FROM pago);
-- Realiza una consulta con el nombre de los clientes y el nombre de 
-- sus representantes junto con la ciudad de la oficina a la que 
-- pertenece el representante.
SELECT c.nombre_cliente, e.nombre, o.ciudad
FROM cliente c, empleado e, oficina o
WHERE c.codigo_empleado_rep_ventas=e.codigo_empleado AND e.codigo_oficina=o.codigo_oficina;
-- Realiza una consulta con la dirección de las oficinas que tengan 
-- clientes en Fuenlabrada.
SELECT o.linea_direccion1, o.linea_direccion2
FROM oficina o, empleado e, cliente c
WHERE o.codigo_oficina=e.codigo_oficina AND e.codigo_empleado=c.codigo_empleado_rep_ventas 
AND c.ciudad="Fuenlabrada";
-- Realiza una consulta con un listado de las diferentes gamas de 
-- producto que ha comprado cada cliente.
SELECT DISTINCT c.nombre_cliente, g.gama
FROM cliente c, pedido ped, detalle_pedido d, producto pro, gama_producto g
WHERE c.codigo_cliente=ped.codigo_cliente AND ped.codigo_pedido=d.codigo_pedido AND
d.codigo_producto=pro.codigo_producto AND pro.gama=g.gama;
-- Realiza una consulta con el nombre de los clientes que hayan 
-- realizado pagos junto con el nombre de sus representantes de ventas.
SELECT DISTINCT c.nombre_cliente, e.nombre
FROM cliente c, empleado e, pago p
WHERE c.codigo_empleado_rep_ventas=e.codigo_empleado AND c.codigo_cliente=p.codigo_cliente;
-- Realiza una consulta con un listado con el nombre de los empleados 
-- junto con el nombre de sus jefes.
SELECT CONCAT_WS(" ",e1.nombre, e1.apellido1, e1.apellido2) Empleado, CONCAT_WS(" ",e2.nombre, e2.apellido1, e2.apellido2) Jefe
FROM empleado e1, empleado e2
WHERE e1.codigo_jefe=e2.codigo_empleado;
-- Realiza una consulta con el nombre de cada cliente y el nombre 
-- y apellido de su representante de ventas.
SELECT c.nombre_cliente, e.nombre, e.apellido1, e.apellido2
FROM cliente c, empleado e
WHERE c.codigo_empleado_rep_ventas=e.codigo_empleado;
-- Realiza una consulta con el nombre de los clientes a los que 
-- no se les ha entregado a tiempo un pedido.
SELECT DISTINCT c.nombre_cliente
FROM cliente c, pedido p
WHERE c.codigo_cliente=p.codigo_cliente AND fecha_esperada<fecha_entrega;