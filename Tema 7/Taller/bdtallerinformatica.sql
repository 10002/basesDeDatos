DROP DATABASE IF EXISTS taller;
CREATE DATABASE taller CHARACTER SET utf8mb4;
USE taller;

CREATE TABLE proveedor (
  codigo INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  nombre VARCHAR(100) NOT NULL
);

CREATE TABLE articulo (
  codigo INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  nombre VARCHAR(100) NOT NULL,
  precio DOUBLE NOT NULL,
  codigo_proveedor INT UNSIGNED NOT NULL,
  FOREIGN KEY (codigo_proveedor) REFERENCES proveedor(codigo)
);

INSERT INTO proveedor VALUES(1, 'Asus');
INSERT INTO proveedor VALUES(2, 'Lenovo');
INSERT INTO proveedor VALUES(3, 'Hewlett-Packard');
INSERT INTO proveedor VALUES(4, 'Samsung');
INSERT INTO proveedor VALUES(5, 'Seagate');
INSERT INTO proveedor VALUES(6, 'Crucial');
INSERT INTO proveedor VALUES(7, 'Gigabyte');
INSERT INTO proveedor VALUES(8, 'Huawei');
INSERT INTO proveedor VALUES(9, 'Xiaomi');

INSERT INTO articulo VALUES(1, 'Impresora HP Deskjet 3720', 59.99, 3);
INSERT INTO articulo VALUES(2, 'Disco SSD 1 TB', 150.99, 4);
INSERT INTO articulo VALUES(3, 'Memoria RAM DDR4 8GB', 120, 6);
INSERT INTO articulo VALUES(4, 'GeForce GTX 1080 Xtreme', 755, 6);
INSERT INTO articulo VALUES(5, 'Monitor 27 LED Full HD', 245.99, 1);
INSERT INTO articulo VALUES(6, 'Disco duro SATA3 1TB', 86.99, 5);
INSERT INTO articulo VALUES(7, 'Portátil Yoga 520', 559, 2);
INSERT INTO articulo VALUES(8, 'Monitor 24 LED Full HD', 202, 1);
INSERT INTO articulo VALUES(9, 'Portátil Ideapd 320', 444, 2);
INSERT INTO articulo VALUES(10, 'Impresora HP Laserjet Pro M26nw', 180, 3);
INSERT INTO articulo VALUES(11, 'GeForce GTX 1050Ti', 185, 7);
