-- Realiza una consulta con el nombre del artículo, precio y nombre de 
-- proveedor de todos los artículos de la base de datos.
SELECT a.nombre articulo, a.precio, p.nombre proveedor
FROM articulo a, proveedor p
WHERE a.codigo_proveedor=p.codigo;
-- Realiza una consulta con el nombre del artículo, precio y nombre de
-- proveedor de todos los artículos de la base de datos. Ordene el
-- resultado por el nombre del proveedor, por orden alfabético.
SELECT a.nombre articulo, a.precio, p.nombre proveedor
FROM articulo a, proveedor p
WHERE a.codigo_proveedor=p.codigo
ORDER BY p.nombre;
-- Realiza una consulta con el código del artículo, nombre del artículo,
-- código del proveedor y nombre del proveedor, de todos los artículos
-- de la base de datos.
SELECT a.codigo cod_articulo, a.nombre nom_articulo, p.codigo cod_proveedor, p.nombre nom_proveedor
FROM articulo a, proveedor p
WHERE a.codigo_proveedor=p.codigo;
-- Realiza una consulta con el nombre del artículo, su precio y el
-- nombre de su proveedor, del artículo más barato.
SELECT a.nombre articulo, a.precio, p.nombre proveedor
FROM articulo a, proveedor p
WHERE a.codigo_proveedor=p.codigo
ORDER BY a.precio 
LIMIT 1;
-- Realiza una consulta con el nombre del artículo, su precio y el
-- nombre de su proveedor, del artículo más caro.
SELECT a.nombre articulo, a.precio, p.nombre proveedor
FROM articulo a, proveedor p
WHERE a.codigo_proveedor=p.codigo
ORDER BY a.precio DESC
LIMIT 1;
-- Realiza una consulta de todos los artículos del proveedor Lenovo.
SELECT a.*
FROM articulo a, proveedor p
WHERE a.codigo_proveedor=p.codigo AND p.nombre="Lenovo";
-- Realiza una consulta de todos los artículos del proveedor Crucial
-- que tengan un precio mayor que 200€.
SELECT a.*
FROM articulo a, proveedor p
WHERE a.codigo_proveedor=p.codigo AND p.nombre="Crucial" AND a.precio>200;
-- Realiza una consulta con todos los artículos de los proveedores
-- Asus, Hewlett-Packard y Seagate. Sin utilizar el operador IN.
SELECT a.*
FROM articulo a, proveedor p
WHERE a.codigo_proveedor=p.codigo AND (p.nombre="Asus" OR p.nombre="Hewlett-Packard" OR p.nombre="Seagate");
-- Realiza una consulta con todos los artículos de los proveedores
-- Asus, Hewlett-Packard y Seagate. Utilizando el operador IN.
SELECT a.*
FROM articulo a, proveedor p
WHERE a.codigo_proveedor=p.codigo AND p.nombre IN ("Asus", "Hewlett-Packard", "Seagate");
-- Realiza una consulta con el nombre y el precio de todos los
-- artículos de los proveedores cuyo nombre termine por la vocal e.
SELECT a.nombre, a.precio
FROM articulo a, proveedor p
WHERE a.codigo_proveedor=p.codigo AND p.nombre LIKE "%e";
-- Realiza una consulta con el nombre y el precio de todos los
-- artículos cuyo nombre de proveedor contenga el carácter w en su nombre.
SELECT a.nombre, a.precio
FROM articulo a, proveedor p
WHERE a.codigo_proveedor=p.codigo AND p.nombre LIKE "%w%";
-- Realiza una consulta con el nombre de artículo, precio y nombre de
-- proveedor, de todos los artículos que tengan un precio mayor o igual a 180€.
-- Ordene el resultado en primer lugar por el precio (en orden descendente)
-- y en segundo lugar por el nombre (en orden ascendente)
SELECT a.nombre articulo, a.precio, p.nombre proveedor
FROM articulo a, proveedor p
WHERE a.codigo_proveedor=p.codigo AND a.precio>=180
ORDER BY a.precio DESC, a.nombre ASC;
-- Realiza una consulta con el código y el nombre de proveedor, solamente
-- de aquellos proveedores que tienen artículos asociados en la base de datos.
SELECT DISTINCT p.codigo, p.nombre
FROM articulo a, proveedor p
WHERE a.codigo_proveedor=p.codigo;