-- Eduardo Jiménez Bahíllo

-- 1. Lista el primer apellido de todos los trabajadores.
SELECT ape1 FROM trabajador;
-- 2. Lista el primer apellido de los trabajadores eliminando los apellidos que estén repetidos.
SELECT DISTINCT ape1 FROM trabajador;
SELECT ape1 FROM trabajador GROUP BY ape1;
-- 3. Lista todas las columnas de la tabla trabajador.
SELECT * FROM trabajador;
-- 4. Lista el nombre y los apellidos de todos los trabajadores.
SELECT nombre, ape1, ape2 FROM trabajador;
-- 5. Lista el código de los departamentos de los trabajadores que aparecen en la tabla trabajador.
SELECT departamento FROM trabajador;
-- 6. Lista el código de los departamentos de los trabajadores que aparecen en la tabla trabajador, eliminando los códigos que aparecen repetidos.
SELECT DISTINCT departamento FROM trabajador;
SELECT departamento FROM trabajador GROUP BY departamento;
-- 7. Lista el nombre y apellidos de los trabajadores en una única columna.
SELECT CONCAT_WS(" ",nombre,ape1,ape2) nombreCompleto FROM trabajador;
-- 8. Lista el nombre y apellidos de los trabajadores en una única columna, convirtiendo todos los caracteres en mayúscula.
SELECT UPPER(CONCAT_WS(" ",nombre,ape1,ape2)) nombreCompleto FROM trabajador;
-- 9. Lista el nombre y apellidos de los trabajadores en una única columna, convirtiendo todos los caracteres en minúscula.
SELECT LOWER(CONCAT_WS(" ",nombre,ape1,ape2)) nombreCompleto FROM trabajador;
-- 10. Lista el nombre de cada departamento y el valor del presupuesto actual del que dispone. Para calcular este dato tendrá que restar al valor del presupuesto 
-- inicial (columna presupuesto) los gastos que se han generado (columna gastos). Tenga en cuenta que en algunos casos pueden existir valores negativos. Utilice 
-- un alias apropiado para la nueva columna columna que está calculando.
SELECT nombre, (presupuesto-gastos) presupuestoActual FROM departamento;
-- 11. Lista el nombre de los departamentos y el valor del presupuesto actual ordenado de forma ascendente.
SELECT nombre, (presupuesto-gastos) presupuestoActual FROM departamento ORDER BY presupuestoActual;
-- 12. Lista el nombre de todos los departamentos ordenados de forma ascendente.
SELECT nombre FROM departamento ORDER BY nombre;
-- 13. Lista el nombre de todos los departamentos ordenados de forma descendente.
SELECT nombre FROM departamento ORDER BY nombre DESC;
-- 14. Lista los apellidos y el nombre de todos los trabajadores, ordenados de forma alfabética tendiendo en cuenta en primer lugar sus apellidos y luego su nombre.
SELECT ape1, ape2, nombre FROM trabajador ORDER BY ape1, ape2, nombre; 
-- 15. Devuelve una lista con el nombre y el presupuesto, de los 3 departamentos que tienen mayor presupuesto.
SELECT nombre, presupuesto FROM departamento ORDER BY presupuesto DESC LIMIT 3;
-- 16. Devuelve una lista con el nombre y el presupuesto, de los 3 departamentos que tienen menor presupuesto.
SELECT nombre, presupuesto FROM departamento ORDER BY presupuesto ASC LIMIT 3;
-- 17. Devuelve una lista con el nombre y el gasto, de los 2 departamentos que tienen mayor gasto.
SELECT nombre, gastos FROM departamento ORDER BY gastos DESC LIMIT 2;
-- 18. Devuelve una lista con el nombre y el gasto, de los 2 departamentos que tienen menor gasto.
SELECT nombre, gastos FROM departamento ORDER BY gastos ASC LIMIT 2;
-- 19. Devuelve una lista con 5 filas a partir de la tercera fila de la tabla trabajador. La tercera fila se debe incluir en la respuesta. 
-- La respuesta debe incluir todas las columnas de la tabla trabajador.
SELECT * FROM trabajador LIMIT 2, 5;
-- 20. Devuelve una lista con el nombre de los departamentos y el presupuesto, de aquellos que tienen un presupuesto mayor o igual a 150000 euros.
SELECT nombre, presupuesto FROM departamento WHERE presupuesto>=150000;
-- 21. Devuelve una lista con el nombre de los departamentos y el gasto, de aquellos que tienen menos de 5000 euros de gastos.
SELECT nombre, gastos FROM departamento WHERE gastos<5000;
-- 22. Devuelve una lista con el nombre de los departamentos y el presupuesto, de aquellos que tienen un presupuesto entre 100000 y 200000 euros. 
-- Sin utilizar el operador BETWEEN.
SELECT nombre, presupuesto FROM departamento WHERE presupuesto>=100000 AND presupuesto<=200000;
-- 23. Devuelve una lista con el nombre de los departamentos que no tienen un presupuesto entre 100000 y 200000 euros. Sin utilizar el operador BETWEEN.
SELECT nombre FROM departamento WHERE NOT (presupuesto>=100000 AND presupuesto<=200000);
-- 24. Devuelve una lista con el nombre de los departamentos que tienen un presupuesto entre 100000 y 200000 euros. Utilizando el operador BETWEEN.
SELECT nombre FROM departamento WHERE presupuesto BETWEEN 100000 AND 200000;
-- 25. Devuelve una lista con el nombre de los departamentos que no tienen un presupuesto entre 100000 y 200000 euros. Utilizando el operador BETWEEN.
SELECT nombre FROM departamento WHERE presupuesto NOT BETWEEN 100000 AND 200000;
-- 26. Devuelve una lista con el nombre de los departamentos, gastos y presupuesto, de aquellos departamentos donde los gastos sean mayores que 
-- el presupuesto del que disponen.
SELECT nombre, gastos, presupuesto FROM departamento WHERE gastos>presupuesto;
-- 27. Devuelve una lista con el nombre de los departamentos, gastos y presupuesto, de aquellos departamentos donde los gastos sean menores que 
-- el presupuesto del que disponen.
SELECT nombre, gastos, presupuesto FROM departamento WHERE gastos<presupuesto;
-- 28. Devuelve una lista con el nombre de los departamentos, gastos y presupuesto, de aquellos departamentos donde los gastos sean iguales 
-- al presupuesto del que disponen.
SELECT nombre, gastos, presupuesto FROM departamento WHERE gastos=presupuesto;
-- 29. Lista todos los datos de los trabajadores cuyo segundo apellido sea NULL.
SELECT * FROM trabajador WHERE ape2 IS NULL;
-- 30. Lista todos los datos de los trabajadores cuyo segundo apellido no sea NULL.
SELECT * FROM trabajador WHERE ape2 IS NOT NULL;
-- 31. Lista todos los datos de los trabajadores cuyo segundo apellido sea López.
SELECT * FROM trabajador WHERE ape2="López";
-- 32. Lista todos los datos de los trabajadores cuyo segundo apellido sea Díaz o Moreno. Sin utilizar el operador IN.
SELECT * FROM trabajador WHERE ape2="Díaz" OR ape2="Moreno";
-- 33. Lista todos los datos de los trabajadores cuyo segundo apellido sea Díaz o Moreno. Utilizando el operador IN.
SELECT * FROM trabajador WHERE ape2 IN ("Díaz","MORENO");
-- 34. Lista los nombres, apellidos y nif de los trabajadores que trabajan en el departamento 3.
SELECT nombre, ape1, ape2, nif FROM trabajador WHERE departamento=3;
-- 35. Lista los nombres, apellidos y nif de los trabajadores que trabajan en los departamentos 2, 4 o 5.
SELECT nombre, ape2, ape2, nif FROM trabajador WHERE departamento IN (2,4,5);