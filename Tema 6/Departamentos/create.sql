-- Eduardo Jiménez Bahíllo

DROP DATABASE IF EXISTS departamentos;
CREATE DATABASE departamentos;

USE departamentos;

CREATE TABLE IF NOT EXISTS departamento (
	codigo INT UNSIGNED AUTO_INCREMENT,
    nombre VARCHAR(100) NOT NULL,
    presupuesto DOUBLE UNSIGNED NOT NULL,
    gastos DOUBLE UNSIGNED NOT NULL,
    PRIMARY KEY (codigo)
);

CREATE TABLE IF NOT EXISTS trabajador (
	codigo INT UNSIGNED AUTO_INCREMENT,
	nif VARCHAR(8) NOT NULL UNIQUE,
    nombre VARCHAR(100) NOT NULL,
    ape1 VARCHAR(100) NOT NULL,
    ape2 VARCHAR(100),
    departamento INT UNSIGNED,
    PRIMARY KEY (codigo),
    FOREIGN KEY (departamento) REFERENCES departamento(codigo) ON UPDATE CASCADE
);