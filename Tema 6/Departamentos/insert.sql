USE departamentos;

INSERT INTO departamento VALUES(1, 'Systems', 150000, 21000);
INSERT INTO departamento VALUES(2, 'Development', 120000, 6000);
INSERT INTO departamento VALUES(3, 'Accounting', 110000, 3000);
INSERT INTO departamento VALUES(4, 'Human Resources', 280000, 25000);
INSERT INTO departamento VALUES(5, 'Projects', 0, 0);
INSERT INTO departamento VALUES(6, 'R&D', 375000, 380000);
INSERT INTO departamento VALUES(7, 'Advertising', 0, 1000);
-- INSERT INTO departamento VALUES(-7, 'Debería dar error', -2, -999);

INSERT INTO trabajador VALUES(1, '12345678', 'Francisco Javier', 'Sánchez', 'Dúo', 1);
INSERT INTO trabajador VALUES(2, '87654321', 'Juan Carlos', 'Terrón', 'Segado', 2);
INSERT INTO trabajador VALUES(3, '98765432', 'Julio', 'Ferrero', 'Pérez', 3);
INSERT INTO trabajador VALUES(4, '23456789', 'Simón', 'Suárez', NULL, 4);
INSERT INTO trabajador VALUES(5, '12378945', 'Eduardo', 'Loyola', 'Méndez', 5);
INSERT INTO trabajador VALUES(6, '32198765', 'Adrián', 'Santana', 'Moreno', 1);
INSERT INTO trabajador VALUES(7, '45678912', 'Raúl', 'Ruiz', NULL, 2);
INSERT INTO trabajador VALUES(8, '65498732', 'Yassin', 'Ahmed', 'Mohamed', 3);
INSERT INTO trabajador VALUES(9, '01234567', 'Nizar', 'Ahmed', 'Mohamed', 2);
INSERT INTO trabajador VALUES(10, '09876543', 'Eduardo','Flores', 'Salas', 5);
INSERT INTO trabajador VALUES(11, '23789456', 'Raquel','Herrera', 'Gil', 1);
INSERT INTO trabajador VALUES(12, '45789123', 'Pedro','Palas', 'Flores', NULL);
INSERT INTO trabajador VALUES(13, '45109639', 'Sergio','Duarte', 'Guerrero', NULL);
-- INSERT INTO trabajador VALUES(14, '45109639', 'Debería','Dar', 'Error', NULL);