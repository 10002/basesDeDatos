-- Eduardo Jiménez Bahíllo

CREATE DATABASE IF NOT EXISTS cesiones;

CREATE TABLE IF NOT EXISTS cesiones.asociacion (
	id INT AUTO_INCREMENT,
	cif VARCHAR(9) NOT NULL UNIQUE,
	email VARCHAR(50) NOT NULL,
	nombre VARCHAR(30) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS cesiones.telefonos (
	numero VARCHAR(15),
	asociacion INT NOT NULL,
	PRIMARY KEY (numero),
	FOREIGN KEY (asociacion) REFERENCES asociacion(id) ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS cesiones.empresa (
	id INT AUTO_INCREMENT,
	cif VARCHAR(9) NOT NULL UNIQUE,
	nombre VARCHAR(30) NOT NULL,
	direccion VARCHAR(80) NOT NULL,
	telefono VARCHAR(15) NOT NULL,
	email VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS cesiones.agente_inm (
	id INT AUTO_INCREMENT,
	nif VARCHAR(9) NOT NULL UNIQUE,
	nombre VARCHAR(30) NOT NULL,
	ape1 VARCHAR(30) NOT NULL,
	ape2 VARCHAR(30) NOT NULL,
	direccion VARCHAR(80) NOT NULL,
	telefono VARCHAR(15) NOT NULL,
	email VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS cesiones.cp_local (
	cp INT,
	poblacion VARCHAR(30),
	PRIMARY KEY (cp)
);

CREATE TABLE IF NOT EXISTS cesiones.local (
	id INT AUTO_INCREMENT,
	calle VARCHAR(30) NOT NULL,
	numero INT,
	piso INT,
	descripcion TEXT NOT NULL,
	cod_postal INT NOT NULL,
	agente_inm INT NOT NULL,
	empresa INT NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (cod_postal) REFERENCES cp_local(cp) ON UPDATE CASCADE,
	FOREIGN KEY (agente_inm) REFERENCES agente_inm(id) ON UPDATE CASCADE,
	FOREIGN KEY (empresa) REFERENCES empresa(id) ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS cesiones.cesion (
	id INT AUTO_INCREMENT,
	fec_ini DATE NOT NULL,
	fec_fin DATE NOT NULL,
	fec_firma DATE,
	remun DECIMAL(8,2),
	fianza DECIMAL(8,2),
	renov BOOLEAN NOT NULL,
	asociacion INT NOT NULL,
	local INT NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (asociacion) REFERENCES asociacion(id) ON UPDATE CASCADE,
	FOREIGN KEY (local) REFERENCES local(id) ON UPDATE CASCADE,
	UNIQUE (local, fec_ini)
);