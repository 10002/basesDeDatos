CREATE DATABASE IF NOT EXISTS webserver;

CREATE TABLE IF NOT EXISTS webserver.usuario (
	login VARCHAR(12),
	pass VARCHAR(16) NOT NULL,
	email VARCHAR(50) NOT NULL,
	fnac DATETIME,
	PRIMARY KEY (login)
);

CREATE TABLE IF NOT EXISTS webserver.blog (
	url VARCHAR(80),
	titulo VARCHAR(20) NOT NULL,
	tipo VARCHAR(20) NOT NULL,
	PRIMARY KEY (url)
);

CREATE TABLE IF NOT EXISTS webserver.usuario_blog (
	usuario VARCHAR(12),
	blog VARCHAR(80),
	PRIMARY KEY (usuario, blog),
	FOREIGN KEY (usuario) REFERENCES usuario(login) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (blog) REFERENCES blog(url) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS webserver.entrada (
	blog VARCHAR(80),
	num_entrada INT(6),
	fecha DATE NOT NULL,
	hora TIME NOT NULL,
	titulo VARCHAR(20) NOT NULL,
	foto VARCHAR(80),
	contenido TEXT,
	categoria VARCHAR(20) NOT NULL,
	PRIMARY KEY (blog, num_entrada),
	FOREIGN KEY (blog) REFERENCES blog(url)  ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS webserver.etiqueta (
	id INT(4) AUTO_INCREMENT,
	nombre VARCHAR(20) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS webserver.entrada_etiquetas (
	blog VARCHAR(80),
	entrada INT(6),
	etiqueta INT(4),
	PRIMARY KEY (blog, entrada, etiqueta),
	FOREIGN KEY (blog, entrada) REFERENCES entrada(blog, num_entrada) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (etiqueta) REFERENCES etiqueta(id) ON UPDATE CASCADE ON DELETE CASCADE
);