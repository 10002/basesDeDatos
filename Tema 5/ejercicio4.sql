-- Creación de la base de datos
CREATE DATABASE IF NOT EXISTS webserver;

-- Creación de las tablas
CREATE TABLE IF NOT EXISTS webserver.usuario (
	login VARCHAR(12),
	pass VARCHAR(16) NOT NULL,
	email VARCHAR(50) NOT NULL,
	fnac DATETIME,
	PRIMARY KEY (login)
);

CREATE TABLE IF NOT EXISTS webserver.blog (
	url VARCHAR(80),
	titulo VARCHAR(20) NOT NULL,
	tipo VARCHAR(20) NOT NULL,
	PRIMARY KEY (url)
);

CREATE TABLE IF NOT EXISTS webserver.usuario_blog (
	usuario VARCHAR(12),
	blog VARCHAR(80),
	PRIMARY KEY (usuario, blog),
	FOREIGN KEY (usuario) REFERENCES usuario(login) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (blog) REFERENCES blog(url) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS webserver.entrada (
	blog VARCHAR(80),
	num_entrada INT(6),
	fecha DATE NOT NULL,
	hora TIME NOT NULL,
	titulo VARCHAR(20) NOT NULL,
	foto VARCHAR(80),
	contenido TEXT,
	categoria VARCHAR(20) NOT NULL,
	PRIMARY KEY (blog, num_entrada),
	FOREIGN KEY (blog) REFERENCES blog(url)  ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS webserver.etiqueta (
	id INT(4) AUTO_INCREMENT,
	nombre VARCHAR(20) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS webserver.entrada_etiquetas (
	blog VARCHAR(80),
	entrada INT(6),
	etiqueta INT(4),
	PRIMARY KEY (blog, entrada, etiqueta),
	FOREIGN KEY (blog, entrada) REFERENCES entrada(blog, num_entrada) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (etiqueta) REFERENCES etiqueta(id) ON UPDATE CASCADE ON DELETE CASCADE
);

-- Inserción de datos en las tablas
INSERT INTO webserver.usuario VALUES 
("user1", 	"123456", 	"usuario1@gmail.com", 	"1990-02-25"),
("user2", 	"123456", 	"usuario2@gmail.com", 	"1992-11-15"),
("user3", 	"123456", 	"usuario3@gmail.com", 	"1984-03-05"),
("user4", 	"123456", 	"usuario4@gmail.com", 	"1979-06-05"),
("user5", 	"123456", 	"usuario5@gmail.com", 	"1989-12-01");

INSERT INTO webserver.blog VALUES
("www.miblog1.com", "Blog personal", "Tipo"),
("www.miblog2.com", "Blog personal", "Tipo"),
("www.miblog3.com", "Blog personal", "Tipo"),
("www.miblog4.com", "Blog personal", "Tipo"),
("www.miblog5.com", "Blog personal", "Tipo"),
("www.miblog6.com", "Blog personal", "Tipo");

INSERT INTO webserver.usuario_blog VALUES
("user1", "www.miblog1.com"),
("user1", "www.miblog6.com"),
("user2", "www.miblog2.com"),
("user3", "www.miblog3.com"),
("user4", "www.miblog4.com"),
("user5", "www.miblog5.com");

INSERT INTO webserver.entrada VALUES
("www.miblog1.com", "1", "2020-02-10", "20:00:00", "Título 1", "", "", ""),
("www.miblog1.com", "2", "2020-02-11", "20:00:00", "Título 2", "", "", ""),
("www.miblog2.com", "1", "2020-02-12", "20:00:00", "Título 3", "", "", ""),
("www.miblog3.com", "1", "2020-02-13", "20:00:00", "Título 4", "", "", ""),
("www.miblog4.com", "1", "2020-02-14", "20:00:00", "Título 5", "", "", ""),
("www.miblog5.com", "1", "2020-02-15", "20:00:00", "Título 6", "", "", ""),
("www.miblog5.com", "2", "2020-02-16", "20:00:00", "Título 7", "", "", ""),
("www.miblog5.com", "3", "2020-02-17", "20:00:00", "Título 8", "", "", ""),
("www.miblog6.com", "1", "2020-02-18", "20:00:00", "Título 9", "", "", "");

INSERT INTO webserver.etiqueta (nombre) VALUES
("Etiqueta1"),
("Etiqueta2"),
("Etiqueta3"),
("Etiqueta4"),
("Etiqueta5");

INSERT INTO webserver.entrada_etiquetas VALUES
("www.miblog1.com","1","1"),
("www.miblog1.com","1","3"),
("www.miblog1.com","1","2"),
("www.miblog1.com","2","4"),
("www.miblog2.com","1","1"),
("www.miblog2.com","1","2"),
("www.miblog3.com","1","5"),
("www.miblog4.com","1","3");

-- Modifica el campo que almacena el email del usuario para que pueda almacenar una cadena de hasta 100 carácteres.
ALTER TABLE webserver.usuario MODIFY COLUMN email VARCHAR(100);
-- Consulta los usuarios menores de 30 años.
SELECT * FROM webserver.usuario WHERE fnac >= DATE_SUB(NOW(), INTERVAL 30 YEAR);
-- Consulta todas las entradas que tenga un blog.
SELECT * FROM webserver.entrada WHERE blog = "www.miblog1.com";
-- Consulta todos los blogs cuyo título empiece por A.
SELECT * FROM webserver.blog WHERE titulo lIKE 'A%';
-- Modifica el email de uno de los usuarios a javier.castillo@iespuertasdelcampo.es
UPDATE webserver.usuario SET email="javier.castillo@iespuertasdelcampo.es" WHERE login="user3";
-- Modifica algún registro de la tabla etiqueta.
UPDATE webserver.etiqueta SET nombre="EtiquetaA" WHERE id=5;
-- Modifica algún valor de la clave primaria de la tabla blogs.
UPDATE webserver.blog SET url="www.miblog.com" WHERE url="www.miblog1.com";
-- Elimina todos los valores de la tabla etiquetas.
DELETE FROM webserver.etiqueta WHERE id<>0;