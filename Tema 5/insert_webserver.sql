INSERT INTO webserver.usuario VALUES 
("user1", 	"123456", 	"usuario1@gmail.com", 	"1990-02-25"),
("user2", 	"123456", 	"usuario2@gmail.com", 	"1992-11-15"),
("user3", 	"123456", 	"usuario3@gmail.com", 	"1984-03-05"),
("user4", 	"123456", 	"usuario4@gmail.com", 	"1979-06-05"),
("user5", 	"123456", 	"usuario5@gmail.com", 	"1989-12-01");

INSERT INTO webserver.blog VALUES
("www.miblog1.com", "Blog personal", "Tipo"),
("www.miblog2.com", "Blog personal", "Tipo"),
("www.miblog3.com", "Blog personal", "Tipo"),
("www.miblog4.com", "Blog personal", "Tipo"),
("www.miblog5.com", "Blog personal", "Tipo"),
("www.miblog6.com", "Blog personal", "Tipo");

INSERT INTO webserver.usuario_blog VALUES
("user1", "www.miblog1.com"),
("user1", "www.miblog6.com"),
("user2", "www.miblog2.com"),
("user3", "www.miblog3.com"),
("user4", "www.miblog4.com"),
("user5", "www.miblog5.com");

INSERT INTO webserver.entrada VALUES
("www.miblog1.com", "1", "2020-02-10", "20:00:00", "Título 1", "", "", ""),
("www.miblog1.com", "2", "2020-02-11", "20:00:00", "Título 2", "", "", ""),
("www.miblog2.com", "1", "2020-02-12", "20:00:00", "Título 3", "", "", ""),
("www.miblog3.com", "1", "2020-02-13", "20:00:00", "Título 4", "", "", ""),
("www.miblog4.com", "1", "2020-02-14", "20:00:00", "Título 5", "", "", ""),
("www.miblog5.com", "1", "2020-02-15", "20:00:00", "Título 6", "", "", ""),
("www.miblog5.com", "2", "2020-02-16", "20:00:00", "Título 7", "", "", ""),
("www.miblog5.com", "3", "2020-02-17", "20:00:00", "Título 8", "", "", ""),
("www.miblog6.com", "1", "2020-02-18", "20:00:00", "Título 9", "", "", "");

INSERT INTO webserver.etiqueta (nombre) VALUES
("Etiqueta1"),
("Etiqueta2"),
("Etiqueta3"),
("Etiqueta4"),
("Etiqueta5");

INSERT INTO webserver.entrada_etiquetas VALUES
("www.miblog1.com","1","1"),
("www.miblog1.com","1","3"),
("www.miblog1.com","1","2"),
("www.miblog1.com","2","4"),
("www.miblog2.com","1","1"),
("www.miblog2.com","1","2"),
("www.miblog3.com","1","5"),
("www.miblog4.com","1","3");