-- Modifica el campo que almacena el email del usuario para que pueda almacenar una cadena de hasta 100 carácteres.
ALTER TABLE webserver.usuario MODIFY COLUMN email VARCHAR(100);
-- Consulta los usuarios menores de 30 años.
SELECT * FROM webserver.usuario WHERE fnac >= DATE_SUB(NOW(), INTERVAL 30 YEAR);
-- Consulta todas las entradas que tenga un blog.
SELECT * FROM webserver.entrada WHERE blog = "www.miblog1.com";
-- Consulta todos los blogs cuyo título empiece por A.
SELECT * FROM webserver.blog WHERE titulo lIKE 'A%';
-- Modifica el email de uno de los usuarios a javier.castillo@iespuertasdelcampo.es
UPDATE webserver.usuario SET email="javier.castillo@iespuertasdelcampo.es" WHERE login="user3";
-- Modifica algún registro de la tabla etiqueta.
UPDATE webserver.etiqueta SET nombre="EtiquetaA" WHERE id=5;
-- Modifica algún valor de la clave primaria de la tabla blogs.
UPDATE webserver.blog SET url="www.miblog.com" WHERE url="www.miblog1.com";
-- Elimina todos los valores de la tabla etiquetas.
DELETE FROM webserver.etiqueta WHERE id<>0;