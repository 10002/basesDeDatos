-- Muestra todos los artículos del proveedor Asus cuyo valor es superior a la media del
-- valor de todos sus artículos.
SELECT a.*
FROM articulo a
JOIN proveedor p ON a.codigo_proveedor=p.codigo
WHERE p.nombre="Asus" AND a.precio>(
	SELECT AVG(a2.precio)
    FROM articulo a2
	WHERE a2.codigo_proveedor=p.codigo
);
-- Muestra el código y el nombre del artículo de mayor valor del proveedor Gigabyte.
SELECT a.codigo, a.nombre
FROM articulo a
JOIN proveedor p ON a.codigo_proveedor=p.codigo
WHERE p.nombre="Gigabyte" AND a.precio=(
	SELECT MAX(a2.precio)
    FROM articulo a2
    WHERE a2.codigo_proveedor=p.codigo
);
-- Muestra el nombre del artículo más barato del proveedor Samsung. *
SELECT a.nombre
FROM articulo a
JOIN proveedor p ON a.codigo_proveedor=p.codigo
WHERE p.nombre="Samsung" AND a.precio=(
	SELECT MIN(a2.precio)
    FROM articulo a2
    WHERE a2.codigo_proveedor=p.codigo
);
-- Muestra los nombres de los proveedores que no tienen actualmente ningún artículo en
-- nuestro taller (EMPLEANDO IN o NOT IN).
SELECT p.nombre
FROM proveedor p
WHERE p.codigo NOT IN (
	SELECT a.codigo_proveedor
    FROM articulo a
);
-- Mostrar todos los artículos cuyo precio es menor o igual al artículo más caro del
-- proveedor Asus.
SELECT nombre, precio
FROM articulo
WHERE precio <= (
	SELECT MAX(a.precio)
    FROM articulo a
    JOIN proveedor p ON a.codigo_proveedor=p.codigo
    WHERE p.nombre="Asus"
);
-- Mostrar una relación en la que aparezca la totalidad de los artículos cuyo precio es
-- mayor o igual a la media aritmética de todos los precios de los artículos de su mismo
-- proveedor.
SELECT a.nombre
FROM articulo a
WHERE precio>=(
	SELECT AVG(a2.precio)
    FROM articulo a2
    WHERE a2.codigo_proveedor=a.codigo_proveedor
);
-- Muestra los nombres de los proveedores que poseen algún artículo en nuestro taller
-- (EMPLEANDO IN o NOT IN).
SELECT p.nombre
FROM proveedor p
WHERE p.codigo IN (
	SELECT a.codigo_proveedor
    FROM articulo a
);
-- Mostrar el artículo de mayor precio (NO EMPLEAR MAX, ORDER BY ni LIMIT).
SELECT nombre
FROM articulo
WHERE precio>=ALL(
	SELECT precio
    FROM articulo
);
-- Muestra los nombres de los proveedores que no tienen actualmente ningún artículo en
-- nuestro taller (EMPLEANDO EXISTS o NOT EXISTS).
SELECT p.nombre
FROM proveedor p
WHERE NOT EXISTS (
	SELECT *
    FROM articulo a
    WHERE a.codigo_proveedor=p.codigo
);
-- Mostrar toda la información de los artículos que poseen el mismo precio que el artículo
-- de mayor precio del proveedor Asus. (NO EMPLEAR INNER JOIN).
SELECT *
FROM articulo
WHERE precio=(
	SELECT MAX(a.precio)
    FROM articulo a
    WHERE a.codigo_proveedor=(
		SELECT p.codigo
        FROM proveedor p
        WHERE p.nombre="Asus"
    )
);
-- Muestra el nombre del artículo de mayor precio del proveedor Asus.
SELECT a.nombre
FROM articulo a
JOIN proveedor p ON a.codigo_proveedor=p.codigo
WHERE p.nombre="Asus" AND a.precio=(
	SELECT MAX(a2.precio)
    FROM articulo a2
    WHERE a2.codigo_proveedor=p.codigo
);
-- Muestra los nombres de los proveedores que no tienen actualmente ningún artículo en
-- nuestro taller (EMPLEANDO ALL ó ANY).
SELECT nombre
FROM proveedor
WHERE codigo!=ALL(
	SELECT codigo_proveedor
    FROM articulo
);
-- Muestra el código y el nombre de cada proveedor con el nombre y el precio de su artículo
-- más caro.
SELECT p.codigo, p.nombre proveedor, a.nombre articulo, a.precio
FROM proveedor p
JOIN articulo a ON p.codigo=a.codigo_proveedor
WHERE a.precio>=ALL(
	SELECT a2.precio
    FROM articulo a2
    WHERE a2.codigo_proveedor=p.codigo
);
-- Muestra los nombres de los proveedores que poseen algún artículo en nuestro taller
-- (EMPLEANDO EXISTS o NOT EXISTS).
SELECT p.nombre
FROM proveedor p
WHERE EXISTS (
	SELECT *
    FROM articulo a
    WHERE a.codigo_proveedor=p.codigo
);
-- Muestra una lista ordenada alfabéticamente con todos los nombres de los proveedores así
-- como la cantidad de artículos de aquellos proveedores que poseen el mismo número de
-- artículos que el proveedor Seagate.
SELECT p.nombre, COUNT(*) numArt
FROM proveedor p
JOIN articulo a ON p.codigo=a.codigo_proveedor
GROUP BY p.nombre
HAVING numArt=(
    SELECT COUNT(*)
    FROM proveedor p2
    JOIN articulo a2 ON p2.codigo=a2.codigo_proveedor
    WHERE p2.nombre="Seagate"
)
ORDER BY p.nombre;
-- Mostrar el artículo de menor valor (NO EMPLEAR MIN, ORDER BY ni LIMIT).
SELECT nombre
FROM articulo
WHERE precio<=ALL(
	SELECT precio
    FROM articulo
);
-- Muestra los nombres de los proveedores que poseen algún artículo en nuestro taller
-- (EMPLEANDO ALL ó ANY).
SELECT nombre 
FROM proveedor
WHERE codigo=ANY(
	SELECT codigo_proveedor
    FROM articulo
);
-- Mostrar todos los artículos del proveedor Asus. (NO EMPLEAR INNER JOIN).
SELECT nombre
FROM articulo
WHERE codigo_proveedor= (
	SELECT codigo
    FROM proveedor
    WHERE nombre="Asus"
);