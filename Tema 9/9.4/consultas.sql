-- Mostrar el nombre del cliente con mayor límite de crédito.
SELECT nombre_cliente
FROM cliente
WHERE limite_credito>=ALL(
	SELECT limite_credito
    FROM cliente
);
-- Mostrar el nombre del producto que tenga el precio de venta más caro.
SELECT nombre
FROM producto
WHERE precio_venta>=ALL(
	SELECT precio_venta
    FROM producto
);
-- Mostrar el nombre del producto del que se han vendido más unidades.
SELECT p.nombre
FROM producto p
NATURAL JOIN detalle_pedido d
GROUP BY p.nombre
HAVING SUM(d.cantidad)>=ALL(
	SELECT SUM(cantidad)
    FROM detalle_pedido
    GROUP BY codigo_producto
);
-- Los clientes cuyo límite de crédito sea mayor que los pagos que haya realizado.
-- (NO EMPLEAR INNER JOIN).
SELECT c.nombre_cliente
FROM cliente c
WHERE c.limite_credito>(
	SELECT SUM(p.total)
    FROM pago p
    WHERE p.codigo_cliente=c.codigo_cliente
    GROUP BY codigo_cliente
);
SELECT c.nombre_cliente
FROM cliente c
WHERE c.codigo_cliente=ANY(
	SELECT p.codigo_cliente
    FROM pago p
    GROUP BY p.codigo_cliente
    HAVING SUM(p.total)<c.limite_credito
);
-- Mostrar el producto que más unidades tiene en stock.
SELECT nombre
FROM producto
WHERE cantidad_en_stock>=ALL(
	SELECT cantidad_en_stock
    FROM producto
);
-- Mostrar el producto que menos unidades tiene en stock.
SELECT nombre
FROM producto
WHERE cantidad_en_stock<=ALL(
	SELECT cantidad_en_stock
    FROM producto
);
-- Mostrar el nombre, los apellidos y el email de los empleados que están a cargo de
-- Alberto Soria.
SELECT CONCAT_WS(" ", nombre, apellido1, apellido2) nombreCompleto, email
FROM empleado
WHERE codigo_jefe=(
	SELECT codigo_empleado
    FROM empleado
    WHERE nombre="Alberto" AND apellido1="Soria"
);
-- Mostrar el nombre del cliente con mayor límite de crédito.
SELECT nombre_cliente
FROM cliente
WHERE limite_credito>=ALL(
	SELECT limite_credito
    FROM cliente
);
-- Mostrar el nombre del producto que tenga el precio de venta más caro.
SELECT nombre
FROM producto
WHERE precio_venta>=ALL(
	SELECT precio_venta
    FROM producto
);
-- Mostrar el nombre del producto que tenga el precio de venta más caro.
SELECT nombre
FROM producto
WHERE precio_venta>=ALL(
	SELECT precio_venta
    FROM producto
);
-- Mostrar el producto que menos unidades tiene en stock.
SELECT nombre
FROM producto
WHERE precio_venta=(
	SELECT MAX(precio_venta)
    FROM producto
);
-- Mostrar el nombre, apellido1 y cargo de los empleados que no representen a ningún cliente.
SELECT e.nombre, e.apellido1, e.puesto
FROM empleado e
WHERE NOT EXISTS(
	SELECT c.codigo_cliente
    FROM cliente c
    WHERE c.codigo_empleado_rep_ventas=e.codigo_empleado
);
-- Mostrar una lista que muestre solamente los clientes que no han realizado ningún pago.
SELECT c.nombre_cliente
FROM cliente c
WHERE NOT EXISTS(
	SELECT p.codigo_cliente
    FROM pago p
    WHERE p.codigo_cliente=c.codigo_cliente
);
-- Mostrar una lista que muestre solamente los clientes que sí han realizado ningún pago.
SELECT c.nombre_cliente
FROM cliente c
WHERE EXISTS(
	SELECT p.codigo_cliente
    FROM pago p
    WHERE p.codigo_cliente=c.codigo_cliente
);
-- Mostrar una lista de los productos que nunca han aparecido en un pedido.
SELECT p.nombre
FROM producto p
WHERE NOT EXISTS(
	SELECT d.codigo_pedido
    FROM detalle_pedido d
    WHERE d.codigo_producto=p.codigo_producto
);
-- Mostrar el nombre, apellidos, puesto y teléfono de la oficina de aquellos empleados que
-- no sean representante de ventas de ningún cliente.
SELECT CONCAT_WS(" ", e.nombre, e.apellido1, e.apellido2) nombreCompleto,
e.puesto, o.telefono
FROM empleado e
NATURAL JOIN oficina o
WHERE NOT EXISTS(
	SELECT c.codigo_cliente
    FROM cliente c
    WHERE c.codigo_empleado_rep_ventas=e.codigo_empleado
);
-- Mostrar las oficinas donde no trabajan ninguno de los empleados que hayan sido los
-- representantes de ventas de algún cliente que haya realizado la compra de algún producto
-- de la gama Frutales.
SELECT o.codigo_oficina
FROM oficina o
WHERE o.codigo_oficina NOT IN(
	SELECT e.codigo_oficina
    FROM empleado e
	JOIN cliente c ON e.codigo_empleado=c.codigo_empleado_rep_ventas
    JOIN pedido ped ON c.codigo_cliente=ped.codigo_cliente
	JOIN detalle_pedido d ON ped.codigo_pedido=d.codigo_pedido
	JOIN producto pro ON d.codigo_producto=pro.codigo_producto
	WHERE pro.gama="Frutales"
);
-- Mostrar una lista con los clientes que han realizado algún pedido pero no han realizado
-- ningún pago.
SELECT c.nombre_cliente
FROM cliente c
WHERE c.codigo_cliente IN (
	SELECT ped.codigo_cliente
    FROM pedido ped
) AND c.codigo_cliente NOT IN (
	SELECT pag.codigo_cliente
    FROM pago pag
);
-- Mostrar una lista que muestre solamente los clientes que no han realizado ningún pago.
SELECT c.nombre_cliente
FROM cliente c
WHERE c.codigo_cliente NOT IN (
	SELECT pag.codigo_cliente
    FROM pago pag
);
-- Mostrar una lista que muestre solamente los clientes que sí han realizado ningún pago.
SELECT c.nombre_cliente
FROM cliente c
WHERE c.codigo_cliente IN (
	SELECT pag.codigo_cliente
    FROM pago pag
);
-- Mostrar una lista de los productos que nunca han aparecido en un pedido.
SELECT p.nombre
FROM producto p
WHERE p.codigo_producto NOT IN(
	SELECT d.codigo_producto
    FROM detalle_pedido d
);
-- Mostrar una lista de los productos que han aparecido en un pedido alguna vez.
SELECT p.nombre
FROM producto p
WHERE p.codigo_producto IN(
	SELECT d.codigo_producto
    FROM detalle_pedido d
);