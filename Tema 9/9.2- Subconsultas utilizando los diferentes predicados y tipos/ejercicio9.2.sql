-- Productos con un precio mayor que la media
SELECT *
FROM producto
WHERE precio > (
	SELECT AVG(precio)
	FROM producto
);

-- Productos cuyo precio es mayor a todos los productos de la categoría 2
SELECT nombre
FROM producto
WHERE precio > ALL (
	SELECT precio
	FROM producto
	WHERE cat=2
);

-- Mostrar el identificador de los clientes que hayan comprado productos cuyo precio sea
-- mayor al de algún producto de la categoria 1
SELECT DISTINCT c.cliente
FROM compra c
JOIN producto p ON c.producto=p.id
WHERE p.precio > ANY (
	SELECT precio
	FROM producto
	WHERE cat=1
);

-- Mostrar los clientes si existen más de 5 compras en la base de datos
SELECT *
FROM cliente
WHERE EXISTS (
	SELECT COUNT(*)
	FROM compra
	HAVING COUNT(*)>5
);

-- Mostrar los clientes que hayan comprado algún producto de la categoría 2
SELECT DISTINCT cli.*
FROM cliente cli
JOIN compra com ON cli.id = com.cliente
WHERE com.producto IN (
	SELECT p.id
	FROM producto p
	WHERE p.cat=2
);
