-- Muestra el nombre completo de los empleados que tienen un sueldo menor al sueldo medio de 
-- los empleados del departamento de Informática
SELECT CONCAT_WS(" ", nombre, apellido1, apellido2) Nombre_empleado
FROM empleado
WHERE sueldo < ( 
	SELECT AVG(sueldo) 
	FROM empleado 
	WHERE departamento="Informática"
);
-- Qué empleados cobran un sueldo superior al sueldo máximo que cobra un empleado en la ciudad 
-- de Ceuta. Muestra su nombre completo.
SELECT CONCAT_WS(" ", nombre, apellido1, apellido2) Nombre_empleado
FROM empleado
WHERE sueldo > ( 
	SELECT MAX(sueldo) 
	FROM empleado 
	WHERE ciudad="Ceuta"
);
-- En la empresa, un empleado puede ser jefe de varios. Muestra el codigo de empleado (del 
-- jefe) junto con el número de empleados subordinados que cobran un sueldo por encima de la 
-- media. *
SELECT jefe.dni DNI_jefe, COUNT(jefe.dni) Número_subordinados
FROM empleado emp
JOIN empleado jefe ON emp.jefe=jefe.dni
WHERE emp.sueldo > (
	SELECT AVG(sueldo) 
	FROM empleado 
)
GROUP BY jefe.dni;
-- Muestra el nombre completo de los empleados que tienen un sueldo superior al sueldo que 
-- cobra el jefe de la empresa.
SELECT CONCAT_WS(" ", nombre, apellido1, apellido2) Nombre_empleado
FROM empleado
WHERE sueldo > ( 
	SELECT sueldo 
	FROM empleado 
	WHERE jefe IS NULL
);
-- La empresa tiene un número no conocido de plantas de trabajo. Muestra el nombre completo y 
-- el departamento de los empleados que trabajan en la planta más alta.
SELECT CONCAT_WS(" ", nombre, apellido1, apellido2) Nombre_empleado, departamento
FROM empleado
WHERE plantaTrabajo = ( 
	SELECT MAX(plantaTrabajo) 
	FROM empleado 
);