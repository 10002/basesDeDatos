DROP DATABASE IF EXISTS prueba;
CREATE DATABASE prueba;
USE prueba;

CREATE TABLE empleado (
	dni INT UNSIGNED,
	nombre VARCHAR(100),
	apellido1 VARCHAR(100),
	apellido2 VARCHAR(100),
	fnac DATE,
	email VARCHAR(100),
	tlf INT UNSIGNED,
	sueldo NUMERIC(15,2),
	departamento VARCHAR(100),
	plantaTrabajo INT UNSIGNED,
	ciudad VARCHAR(30),
	jefe INT UNSIGNED,
	PRIMARY KEY (dni),
	FOREIGN KEY (jefe) REFERENCES empleado(dni)
);

INSERT INTO empleado (dni, nombre, apellido1, apellido2, sueldo, departamento, ciudad, plantaTrabajo)
VALUES (1, "Nom1", "Ape1", "Ape1", 1000, "Informática", "Ceuta", 7);
INSERT INTO empleado (dni, nombre, apellido1, apellido2, sueldo, departamento, ciudad, jefe, plantaTrabajo)
VALUES (2, "Nom2", "Ape2", "Ape2", 2000, "Informática", "Ceuta", 1, 3);
INSERT INTO empleado (dni, nombre, apellido1, apellido2, sueldo, departamento, ciudad, jefe, plantaTrabajo)
VALUES (3, "Nom3", "Ape3", "Ape3", 1500, "Informática", "Ceuta", 1, 6);
INSERT INTO empleado (dni, nombre, apellido1, apellido2, sueldo, departamento, ciudad, jefe, plantaTrabajo)
VALUES (4, "Nom4", "Ape4", "Ape4", 1000, "Carnicería", "Ceuta", 1, 1);
INSERT INTO empleado (dni, nombre, apellido1, apellido2, sueldo, departamento, ciudad, jefe, plantaTrabajo)
VALUES (5, "Nom5", "Ape5", "Ape5", 2100, "Carnicería", "Cádiz", 4, 7);
INSERT INTO empleado (dni, nombre, apellido1, apellido2, sueldo, departamento, ciudad, jefe, plantaTrabajo)
VALUES (6, "Nom6", "Ape6", "Ape6", 2500, "Informática", "Cádiz", 4, 7);
INSERT INTO empleado (dni, nombre, apellido1, apellido2, sueldo, departamento, ciudad, jefe, plantaTrabajo)
VALUES (7, "Nom7", "Ape7", "Ape7", 1500, "Carnicería", "Cádiz", 4, 2);