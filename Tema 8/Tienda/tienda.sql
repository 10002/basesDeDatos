DROP DATABASE IF EXISTS tienda;
CREATE DATABASE tienda;
USE tienda;

CREATE TABLE cliente (
	id INT,
    nombre VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE categoria (
	id INT NOT NULL,
    nombre VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE producto (
	id INT NOT NULL,
    nombre VARCHAR(100) NOT NULL,
	precio NUMERIC(10,2),
    cat INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (cat) REFERENCES categoria (id)
);

CREATE TABLE compra  (
	id INT,
    cliente INT NOT NULL,
    producto INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (cliente) REFERENCES cliente (id),
    FOREIGN KEY (producto) REFERENCES producto (id)
);

INSERT INTO cliente VALUES(1, 'María');
INSERT INTO cliente VALUES(2, 'Pepe');
INSERT INTO cliente VALUES(3, 'Paco');

INSERT INTO categoria VALUES(1, 'Electrónica');
INSERT INTO categoria VALUES(2, 'Libros');
INSERT INTO categoria VALUES(3, 'Música');
INSERT INTO categoria VALUES(4, 'Videojuegos');

INSERT INTO producto VALUES(1, 'Amazon Fire TV Stick', 30, 1);
INSERT INTO producto VALUES(2, 'Kindle', 99.99, 1);
INSERT INTO producto VALUES(3, 'HUAWEI Watch GT 2e Sport', 120, 1);
INSERT INTO producto VALUES(4, 'Palabras radiantes (El Archivo de las Tormentas 2) ', 32, 2);
INSERT INTO producto VALUES(5, 'El nombre del viento (Crónica del asesino de reyes)', 29.78, 2);
INSERT INTO producto VALUES(6, 'El aliento de los dioses', 22, 2);
INSERT INTO producto VALUES(7, 'The Dark Side Of The Moon', 16, 3);
INSERT INTO producto VALUES(8, 'Gigaton', 14, 3);

INSERT INTO compra VALUES(1, 1, 1);
INSERT INTO compra VALUES(2, 2, 2);
INSERT INTO compra VALUES(3, 1, 2);
INSERT INTO compra VALUES(4, 1, 3);
INSERT INTO compra VALUES(5, 2, 4);
INSERT INTO compra VALUES(6, 1, 5);

-- Consulta con el nombre de todas las categorías y de los productos que pertenecen a cada una de ellas
SELECT c.nombre Categoría, p.nombre Producto
FROM categoria c
LEFT JOIN producto p ON c.id=p.cat;

-- Consulta con el nombre de todos los productos y de los clientes que lo han comprado
SELECT p.nombre Producto, cli.nombre Cliente
FROM cliente cli
JOIN compra com ON cli.id=com.cliente
RIGHT JOIN producto p ON com.producto=p.id;

-- Productos con un precio mayor que la media
SELECT *
FROM producto
WHERE precio > (
	SELECT AVG(precio)
	FROM producto
);

-- Productos cuyo precio es mayor a todos los productos de la categoría 2
SELECT nombre
FROM producto
WHERE precio > ALL (
	SELECT precio
	FROM producto
	WHERE cat=2
);

-- Mostrar el identificador de los clientes que hayan comprado productos cuyo precio sea
-- mayor al de algún producto de la categoria 1
SELECT DISTINCT c.cliente
FROM compra c
JOIN producto p ON c.producto=p.id
WHERE p.precio > ANY (
	SELECT precio
	FROM producto
	WHERE cat=1
);

-- Mostrar los clientes si existen más de 5 compras en la base de datos
SELECT *
FROM cliente
WHERE EXISTS (
	SELECT COUNT(*)
	FROM compra
	HAVING COUNT(*)>5
);

-- Mostrar los clientes que hayan comprado algún producto de la categoría 2
SELECT DISTINCT cli.*
FROM cliente cli
JOIN compra com ON cli.id = com.cliente
WHERE com.producto IN (
	SELECT p.id
	FROM producto p
	WHERE p.cat=2
);
